export default [
  {
    catalog_Name: "Catalog 1",
    view_all_text: "View All",
    catalog_id: 1,
  },
  {
    catalog_Name: "Catalog 2",
    view_all_text: "View All",
    catalog_id: 2,
  },
  {
    catalog_Name: "Catalog 3",
    view_all_text: "View All",
    catalog_id: 3,
  },
]