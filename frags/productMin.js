export default [{
    id: "1",
    // status: "Новинка",
    // prod_status: "Нет в наличии",
    prod_new: true,
    prod_stock: false,
    statusNo: true,
    statusYes: false,
    catalog_id: 1,
    category: "Вело тренажер",
    name: "aLifeTop LTsss sss sss sss sss sss sssss",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 1,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "2",
    prod_new: false,
    prod_stock: true,
    statusNo: false,
    statusYes: true,
    catalog_id: 1,
    category: "Вело тренажер",
    name: "aNew Noble V3",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 2,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "3",
    prod_new: true,
    prod_stock: false,
    statusNo: true,
    statusYes: false,
    catalog_id: 1,
    category: "Вело тренажер",
    name: "aNew Noble V1",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 3,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "4",
    prod_new: false,
    prod_stock: true,
    statusNo: false,
    statusYes: true,
    catalog_id: 1,
    category: "Вело тренажер",
    name: "aNew Noble V5",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 4,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "5",
    prod_new: true,
    prod_stock: false,
    statusNo: true,
    statusYes: false,
    catalog_id: 1,
    category: "Вело тренажер",
    name: "aNew Noble V6",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 5,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "6",
    prod_new: false,
    prod_stock: true,
    statusNo: false,
    statusYes: true,
    catalog_id: 2,
    category: "Вело тренажер",
    name: "aSprinter 1500",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 2,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "7",
    prod_new: true,
    prod_stock: false,
    statusNo: true,
    statusYes: false,
    catalog_id: 2,
    category: "Вело тренажер",
    name: "aSprinter 360-B",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 4,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "8",
    prod_new: false,
    prod_stock: true,
    statusNo: false,
    statusYes: true,
    catalog_id: 2,
    category: "Вело тренажер",
    name: "aSprinter 12AF",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 1,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "9",
    prod_new: true,
    prod_stock: false,
    statusNo: true,
    statusYes: false,
    catalog_id: 2,
    category: "Вело тренажер",
    name: "aFitland 6001",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 5,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "10",
    prod_new: false,
    prod_stock: true,
    statusNo: false,
    statusYes: true,
    catalog_id: 2,
    category: "Вело тренажер",
    name: "aFitland 1001",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 5,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "11",
    prod_new: true,
    prod_stock: false,
    statusNo: true,
    statusYes: false,
    catalog_id: 3,
    category: "Вело тренажер",
    name: "FITLAND 07",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 3,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "12",
    prod_new: false,
    prod_stock: true,
    statusNo: false,
    statusYes: true,
    catalog_id: 3,
    category: "Вело тренажер",
    name: "New Noble V12",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 3,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],

    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "13",
    prod_new: true,
    prod_stock: false,
    statusNo: true,
    statusYes: false,
    catalog_id: 3,
    category: "Вело тренажер",
    name: "New Noble 4500",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 1,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],
    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "14",
    prod_new: false,
    prod_stock: true,
    statusNo: false,
    statusYes: true,
    catalog_id: 3,
    category: "Вело тренажер",
    name: "New Noble 4500",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 1,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],
    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },
  {
    id: "15",
    prod_new: true,
    prod_stock: false,
    statusNo: true,
    statusYes: false,
    catalog_id: 3,
    category: "Вело тренажер",
    name: "New Noble 4500",
    price: "1.500.000",
    oldPrice: "2.000.000",
    reviews: 1,
    totalProductNumber: "10",
    slug: 'fitland-14',
    images: [
      require("~/assets/images/product_imeges/3.png"),
      require("~/assets/images/product_imeges/1.png"),
      require("~/assets/images/product_imeges/2.png"),
      require("~/assets/images/product_imeges/4.png"),
      require("~/assets/images/product_imeges/5.png"),
      require("~/assets/images/product_imeges/6.png"),
    ],
    characteristics: [{
        name: "Максимальная вес пользователя:",
        atribute: "150 кг",
      },
      {
        name: "Размер бегового полотно:",
        atribute: "1-20 км/ч",
      },
      {
        name: "Диапазон скоростей:",
        atribute: "4.5 hp",
      },
      {
        name: "Диапазон наклона:",
        atribute: "520х1480 мм",
      },
      {
        name: "DC матор:",
        atribute: " 0-18 %",
      },
    ],
    Descriptions: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Diam consequat ac, faucibus nulla viverra amet eleifend. Suspendisse vel egestas tellus volutpat amet pellentesque.Suspendisse vel egestas tellus volutpat amet pellentesque. Suspendisse vel egestas tellus volutpat amet `
  },

]
